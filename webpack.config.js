const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// var s = require('./node_modules/smooth-scroll/dist/js/smooth-scroll');

module.exports = function(env) {
    return {
        entry: __dirname + "/src/js/app.js",
        // vendor: [s],
        output: {
            path: __dirname + "/custom",
            filename: "app.js"
        },
        module: {
            rules: [
                {test: /\.js$/, loader: 'babel-loader', exclude:/node_modules/, query: {
                  presets: ['es2015']
                }},
                {test: /\.html$/, loader: 'raw-loader', exclude: /node_modules/},
                {test: /\.css$/, loader: "style-loader!css-loader", exclude: /node_modules/},
                {test: /\.scss$/, use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: "css-loader!postcss-loader!sass-loader"
                }), exclude: /node_modules/},
                {test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, loader: 'url-loader'}
            ]
        },
        plugins: [
          new ExtractTextPlugin('styles.css')
        ],
        watch: true,
    }
}
