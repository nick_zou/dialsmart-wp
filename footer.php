<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DialSmart
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/DialSmartlogo-whitegreen.png">
			<h6>&copy; DialSmart | Developed by <a href="https://leverage.it/">LeverageIT</a></h6>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
