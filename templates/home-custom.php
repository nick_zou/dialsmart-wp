<?php
/**
 * Template Name: Home Custom
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package DialSmart
 */

get_header();

?>

<div id="primary" class="content-area">
<main id="main" class="site-main">


  <?php
  if ( have_posts() ) :

    if ( is_home() && ! is_front_page() ) :
      ?>
      <header>
        <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
      </header>
      <?php
    endif;

    /* Start the Loop */
    while ( have_posts() ) :
      the_post();

      /*
       * Include the Post-Type-specific template for the content.
       * If you want to override this in a child theme, then include a file
       * called content-___.php (where ___ is the Post Type name) and that will be used instead.
       */ ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">

    <!--Hero Section-->

    <div class="container-fluid section jarallax" id="hero">
      <img src="<?php the_field('hero_background_image');?>" class="jarallax-img">
      <div class="row" style="background: url(<?php the_field('hero_background_image'); ?>), linear-gradient(to bottom right, #CA2564, #C2CA20)">
        <div class="container">
          <div class="row">
            <div class="col col-12 col-md-8">
              <div class="hero-section">
                <h1><?php the_field('hero_title'); ?></h1>
                <h5><?php the_field('hero_text'); ?></h5>
                <a href="/#contact">request access</a>
              </div>
            </div>
            <div class="col col-12 col-md-4">
              <div class="hero-image">
                  <img src="<?php the_field('hero_image'); ?>" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--App Download-->
    <div class="container-fluid section" id="download">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col col-12 col-lg-5 order-last order-lg-first">
              <div class="download-image">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/dialoicon.png" />
              </div>
            </div>
            <div class="col col-12 col-lg-7 order-first order-lg-last">
              <div class="download-text">
                <h3><?php the_field('download_title'); ?></h3>
                <p><?php the_field('download_text'); ?></p>
                <div class="download-buttons">
                  <div class="button-container">
                    <a href="<?php the_field('google_download_url'); ?>">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/Google-Play-badge.svg">
                    </a>
                  </div>
                  <div class="button-container">
                    <a href="<?php the_field('app_store_download_url'); ?>">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/Apple-Store-badge.svg">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--What is DialSmart Section-->
    <div class="container section" id="what">
      <div class="row">
        <div class="col col-12 col-lg-8">
          <div class="what-text">
            <h2><?php the_field('what_title'); ?></h2>
            <p><?php the_field('what_text'); ?></p>
          </div>
        </div>
        <div class="col col-12 col-lg-4">
          <div class="what-image">
            <img src="<?php the_field('what_image'); ?>" />
          </div>
        </div>
      </div>
    </div>
    <!--How does DialSmart work Section-->
    <div class="container-fluid section" id="how">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col col-12">
              <div class="how-title">
                <h2><?php the_field('how_title'); ?></h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-12 col-md-4">
              <div class="how-section">
                <div class="image">
                  <img src="<?php the_field('how_image_1'); ?>" />
                </div>
                <div class="text">
                  <h5>1.</h5>
                  <h5><?php the_field('how_text_1'); ?></h5>
                </div>
              </div>
            </div>
            <div class="col col-12 col-md-4">
              <div class="how-section">
                <div class="image">
                  <img src="<?php the_field('how_image_2'); ?>" />
                </div>
                <div class="text">
                  <h5>2.</h5>
                  <h5><?php the_field('how_text_2'); ?></h5>
                </div>
              </div>
            </div>
            <div class="col col-12 col-md-4">
              <div class="how-section">
                <div class="image">
                  <img src="<?php the_field('how_image_3'); ?>" />
                </div>
                <div class="text">
                  <h5>3.</h5>
                  <h5><?php the_field('how_text_3'); ?></h5>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-12">
              <div class="how-button">
                <a href="#" class="button">Get DialSmart for your business</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Do more with your inbound calls Section-->
    <div class="container-fluid section" id="more">
      <div class="row bg-magenta">
        <div class="container">
          <div class="row">
            <div class="col col-12">
              <div class="more-title">
                <h2><?php the_field('more_title'); ?></h2>
              </div>
            </div>
          </div>
          <div class="row justify-content-lg-end">
            <div class="col col-md-12 col-lg-6">
              <div class="more-section-1">
                <div class="more-icons">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/do-icon-1.svg">
                </div>
                <div class="more-icons">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/do-icon-2.svg">
                </div>
                <div class="more-icons">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/do-icon-3.svg">
                </div>
                <div class="more-icons">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/do-icon-4.svg">
                </div>
                <div class="more-icons">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/do-icon-5.svg">
                </div>
                <div class="more-icons">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/do-icon-6.svg">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col col-12 col-md-6">
              <div class="more-image">
                <img src="<?php the_field('more_image'); ?>" />
              </div>
            </div>
            <div class="col col-12 col-md-6">
              <div class="more-section-2">
                <?php the_field('more_text'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Benefits of Dialmsart section-->
    <div class="container-fluid section" id="benefits">
      <div class="row">
        <div class="col col-12">
          <div class="benefits-title">
            <h2><?php the_field('benefits_title'); ?></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col col-6">
          <a class="benefits-tab first">
            <h4 class="desktop"><?php the_field('benefits_tab_1'); ?></h4>
            <h4 class="mobile"><?php the_field('benefits_tab_1_responsive'); ?></h4>
            <span class="background-letter"><?php the_field('benefits_tab_letter_1'); ?></span>
          </a>
        </div>
        <div class="col col-6">
          <a class="benefits-tab second">
            <h4 class="desktop"><?php the_field('benefits_tab_2'); ?></h4>
            <h4 class="mobile"><?php the_field('benefits_tab_2_responsive'); ?></h4>
            <span class="background-letter"><?php the_field('benefits_tab_letter_2'); ?></span>
          </a>
        </div>
      </div>
      <div class="row benefits-tab-content">
        <div class="col col-12 benefits-tab-slide first">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_1_feature_image_1'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_1_feature_text_1'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_1_feature_image_2'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_1_feature_text_2'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_1_feature_image_3'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_1_feature_text_3'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_1_feature_image_4'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_1_feature_text_4'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_1_feature_image_5'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_1_feature_text_5'); ?></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-12 benefits-tab-slide second">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_2_feature_image_1'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_2_feature_text_1'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_2_feature_image_2'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_2_feature_text_2'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_2_feature_image_3'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_2_feature_text_3'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_2_feature_image_4'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_2_feature_text_4'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_2_feature_image_5'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_2_feature_text_5'); ?></h6>
                  </div>
                </div>
              </div>
              <div class="col col-12 col-md-6 col-lg-4">
                <div class="benefits-tab-slide-item">
                  <div class="image">
                    <img src="<?php the_field('benefits_tab_2_feature_image_6'); ?>" />
                  </div>
                  <div class="text">
                    <h6><?php the_field('benefits_tab_2_feature_text_6'); ?></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Contact section-->
    <div class="container-fluid section" id="contact">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col col-12">
              <div class="contact-title">
                <h2><?php the_field('contact_title'); ?></h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-12 col-lg-6">
              <div class="contact-download">
                <div class="container">
                  <div class="row justify-content-md-center">
                    <div class="col col-6 col-lg-5 col-xl-4">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/dialoicon-small.png" />
                    </div>
                    <div class="col col-6 col-md-auto col-lg-7">
                      <div class="contact-download-buttons">
                        <a href="<?php the_field('google_download_url'); ?>">
                          <img src="<?php echo get_template_directory_uri(); ?>/assets/Google-Play-badge.svg">
                        </a>
                        <a href="<?php the_field('app_store_download_url'); ?>">
                          <img src="<?php echo get_template_directory_uri(); ?>/assets/Apple-Store-badge.svg">
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col col-12 col-lg-6">
              <div class="contact-form">
                <div class="container">
                  <div class="row">
                    <div class="col col-12">
                      <div class="contact-form-title">
                        <p>Contact us to get started</p>
                      </div>
                    </div>
                    <div class="col col-12">
                      <?php wpforms_display(57); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'dialsmart' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'dialsmart' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php dialsmart_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->


<?php
endwhile;

the_posts_navigation();

else :

get_template_part( 'template-parts/content', 'none' );

endif;
?>

</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
