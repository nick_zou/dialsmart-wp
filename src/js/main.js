let siteHeader = document.getElementById("masthead");
let menuToggle = document.getElementById("menu-toggle");
let siteNav = document.getElementById("site-navigation");
let menuIcon = document.querySelector(".icon");
let menuList = document.querySelector(".nav-menu");
let siteLogo = document.querySelector(".site-branding");
let benefitsTabContainer = document.querySelector(".benefits-tab-content");
let benefitsTabFirst = document.querySelector(".benefits-tab.first");
let benefitsTabSecond = document.querySelector(".benefits-tab.second");
const headerHeight = 80;
var scroll = new SmoothScroll('a[href*="#"]');
import {
    jarallax,
    jarallaxElement,
} from 'jarallax';

jarallaxElement();

jarallax(document.querySelectorAll('.jarallax'), {
    speed: 0.15,
    disableParallax: /iPad|iPhone|iPod|Android/
});

//Mobile Menu
menuToggle.addEventListener('click', function() {
  siteNav.classList.toggle("active");
  if (menuIcon.classList.contains("ion-navicon")) {
    menuIcon.classList.remove("ion-navicon");
    menuIcon.classList.add("ion-android-close");
  } else {
    menuIcon.classList.remove("ion-android-close");
    menuIcon.classList.add("ion-navicon");
  }
  siteLogo.classList.toggle("active");
});

//Scrolled Menu
window.addEventListener('scroll', function() {
  if (window.pageYOffset > headerHeight) {
    siteHeader.classList.add("scrolled");
  }
  else {
    siteHeader.classList.remove("scrolled");
  }
});

menuList.addEventListener('click', function() {
  if (siteNav.classList.contains("active")) {
    siteNav.classList.remove("active");
    siteLogo.classList.remove("active");
  }
});

//Content Tabs
benefitsTabFirst.addEventListener('click', function() {
  if (benefitsTabContainer.classList.contains("second-active")) {
    benefitsTabContainer.classList.remove("second-active");
  }
});
benefitsTabSecond.addEventListener('click', function() {
  if (!benefitsTabContainer.classList.contains("second-active")) {
    benefitsTabContainer.classList.add("second-active");
  }
});
